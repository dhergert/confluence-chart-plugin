package com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.LanguageManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.links.LinkResolver;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestChartMacroPropertyAccessor
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private SettingsManager settingsManager;
    @Mock
    private LanguageManager languageManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ThumbnailManager thumbnailManager;
    @Mock
    private WritableDownloadResourceManager exportDownloadResourceManager;
    @Mock
    private XhtmlContent xhtmlContent;
    @Mock
    private LinkResolver linkResolver;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private I18NBeanFactory i18nBeanFactory;

    private ChartMacro chartMacro;

    @Before
    public void setUp() {
        chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager,
                thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory);
    }

    @Test
    public void testGetNonExistentIntegerParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(new Integer(1), chartMacro.getIntegerParameter(
                new HashMap<String, String>(),
                "anInteger",
                1
        ));
    }

    @Test
    public void testGetNonExistentIntegerParameterWithoutDefault() throws MacroExecutionException
    {
        assertNull(chartMacro.getIntegerParameter(
                new HashMap<String, String>(),
                "anInteger",
                null
        ));
    }

    @Test
    public void testGetExistentIntegerParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(new Integer(-1), chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(-1));
                    }
                },
                "anInteger",
                0
        ));
    }

    @Test
    public void testGetExistentIntegerParameterWithoutDefault() throws MacroExecutionException
    {
        assertEquals(new Integer(-1), chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(-1));
                    }
                },
                "anInteger",
                null
        ));
    }

    @Test(expected = MacroExecutionException.class)
    public void testGetExistentDoubleParameterAsIntegerWithDefault() throws MacroExecutionException {
        chartMacro.getIntegerParameter(
                    new HashMap<String, String>()
                    {
                        {
                            put("anInteger", String.valueOf(1.0));
                        }
                    },
                    "anInteger",
                    0
            );
    }

    @Test
    public void testGetNonExistentIntegerParameterWithThreshold() throws MacroExecutionException
    {
        assertEquals(1, chartMacro.getIntegerParameter(
                new HashMap<String, String>(),
                "anInteger",
                1,
                Integer.MAX_VALUE
        ));
    }

    @Test
    public void testGetExistentIntegerParameterWithLargerThreshold() throws MacroExecutionException
    {
        assertEquals(1, chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(10));
                    }
                },
                "anInteger",
                1,
                11
        ));
    }

    @Test
    public void testGetExistentIntegerParameterWithSmallerThreshold() throws MacroExecutionException
    {
        assertEquals(10, chartMacro.getIntegerParameter(
                new HashMap<String, String>()
                {
                    {
                        put("anInteger", String.valueOf(10));
                    }
                },
                "anInteger",
                1,
                9
        ));
    }

    @Test
    public void testGetNonExistentDoubleParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(new Double(1d), chartMacro.getDoubleParameter(
                new HashMap<String, String>(),
                "aDouble",
                1d
        ));
    }

    @Test
    public void testGetNonExistentDoubleParameterWithoutDefault() throws MacroExecutionException
    {
        assertNull(chartMacro.getDoubleParameter(
                new HashMap<String, String>(),
                "aDouble",
                null
        ));
    }

    @Test
    public void testGetExistentDoubleParameterWithDefault() throws MacroExecutionException
    {
        assertEquals(new Double(-1d), chartMacro.getDoubleParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aDouble", String.valueOf(-1d));
                    }
                },
                "aDouble",
                0d
        ));
    }

    @Test
    public void testGetExistentDoubleParameterWithoutDefault() throws MacroExecutionException
    {
        assertEquals(new Double(-1d), chartMacro.getDoubleParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aDouble", String.valueOf(-1d));
                    }
                },
                "aDouble",
                null
        ));
    }

    @Test(expected = MacroExecutionException.class)
    public void testGetExistentStringParameterAsDoubleWithDefault() throws MacroExecutionException {
       chartMacro.getDoubleParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aDouble", "invalid");
                    }
                },
                "aDouble",
                0d
        );
    }

    @Test
    public void testGetNonExistentBooleanParameter() {
        assertTrue(chartMacro.getBooleanParameter(
                new HashMap<String, String>(),
                "aBoolean",
                true
        ));
    }

    @Test
    public void testGetExistentBooleanParameter() {
        assertFalse(chartMacro.getBooleanParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aBoolean", "false");
                    }
                },
                "aBoolean",
                true
        ));
    }

    @Test
    public void testGetExistentStringParameterAsBoolean() {
        assertTrue(chartMacro.getBooleanParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aBoolean", "shouldResultInTrue");
                    }
                },
                "aBoolean",
                true
        ));
    }

    @Test
    public void testGetNonExistentStringParameterWithDefault() {
        assertEquals("defaultString", chartMacro.getStringParameter(
                new HashMap<String, String>(),
                "aString",
                "defaultString"
        ));
    }

    @Test
    public void testGetNonExistentStringParameterWithoutDefault() {
        assertNull(chartMacro.getStringParameter(
                new HashMap<String, String>(),
                "aString",
                null
        ));
    }

    @Test
    public void testGetExistentStringParameterWithDefault() {
        assertEquals("userSpecifiedString", chartMacro.getStringParameter(
                new HashMap<String, String>()
                {
                    {
                        put("aString", "userSpecifiedString");
                    }
                },
                "aString",
                "defaultString"
        ));
    }
}
