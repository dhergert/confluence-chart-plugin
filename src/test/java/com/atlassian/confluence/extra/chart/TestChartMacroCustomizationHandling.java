package com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.LanguageManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.links.LinkResolver;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickMarkPosition;
import org.jfree.chart.axis.DateTickUnit;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.general.PieDataset;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.stubbing.Answer;

import java.awt.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestChartMacroCustomizationHandling
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private SettingsManager settingsManager;
    @Mock
    private LanguageManager languageManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ThumbnailManager thumbnailManager;
    @Mock
    private WritableDownloadResourceManager exportDownloadResourceManager;
    @Mock
    private XhtmlContent xhtmlContent;
    @Mock
    private LinkResolver linkResolver;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private I18NBeanFactory i18nBeanFactory;

    private ChartMacro chartMacro;

    @Before
    public void setUp() {
        chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager,
                thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory);
    }

    @Test
    public void testHandlePiePlotSectionExplosionCustomization() {
        final String sectionsToExploded = "section1, section2,section3";

        PiePlot piePlot = mock(PiePlot.class);

        chartMacro.handlePiePlotCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("piesectionexplode", sectionsToExploded);
                    }
                },
                piePlot
        );

        verify(piePlot).setExplodePercent("section1", 0.3);
        verify(piePlot).setExplodePercent("section2", 0.3);
        verify(piePlot).setExplodePercent("section3", 0.3);
    }

    @Test
    public void testHandleCategoryNumberPlotNumberRangeCustomization() throws MacroExecutionException
    {
        CategoryPlot categoryPlot = mock(CategoryPlot.class);
        NumberAxis rangeAxis = mock(NumberAxis.class);

        when(categoryPlot.getRangeAxis()).thenReturn(rangeAxis);

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("rangeaxislabelangle", "45");
                        put("rangeaxisrotateticklabel", "true");
                        put("rangeaxislowerbound", String.valueOf(1));
                        put("rangeaxisupperbound", String.valueOf(10));
                        put("rangeaxistickunit", String.valueOf(1));
                        put("datetickmarkposition", "middle");
                    }
                },
                categoryPlot,
                null
        );

        verify(rangeAxis).setLabelAngle(Math.toRadians(45));
        verify(rangeAxis).setVerticalTickLabels(true);
        verify(rangeAxis).setLowerBound(1);
        verify(rangeAxis).setUpperBound(10);
        verify(rangeAxis).setTickUnit(new NumberTickUnit(1));
    }

    @Test
    public void testHandleCategoryNumberPlotDateRangeCustomization() throws MacroExecutionException, ParseException
    {
        CategoryPlot categoryPlot = mock(CategoryPlot.class);
        DateAxis rangeAxis = mock(DateAxis.class);
        ChartData chartData = mock(ChartData.class);

        when(categoryPlot.getRangeAxis()).thenReturn(rangeAxis);

        final DateFormat dateFormatOverride = new SimpleDateFormat("yyyy-MM-dd");
        when(chartData.getDateFormat(anyInt())).thenReturn(dateFormatOverride);

        when(chartData.toDate(anyString())).thenAnswer(
                (Answer<Date>) invocationOnMock -> dateFormatOverride.parse((String) invocationOnMock.getArguments()[0])
        );

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("rangeaxislabelangle", "45");
                        put("rangeaxisrotateticklabel", "true");
                        put("rangeaxislowerbound", "1970-01-01");
                        put("rangeaxisupperbound", "1971-01-01");
                        put("rangeaxistickunit", "3");
                        put("timeperiod", "month");
                        put("datetickmarkposition", "middle");
                    }
                },
                categoryPlot,
                chartData
        );

        verify(rangeAxis).setDateFormatOverride(dateFormatOverride);
        verify(rangeAxis).setLabelAngle(Math.toRadians(45));
        verify(rangeAxis).setVerticalTickLabels(true);
        verify(rangeAxis).setMinimumDate(new DateTime(1970, 1, 1, 0, 0, 0, 0).toDate());
        verify(rangeAxis).setMaximumDate(new DateTime(1971, 1, 1, 0, 0, 0, 0).toDate());
        verify(rangeAxis).setTickMarkPosition(DateTickMarkPosition.MIDDLE);
        verify(rangeAxis).setTickUnit(new DateTickUnit(DateTickUnit.MONTH, 3));
    }

    @Test
    public void testRotateCategoryNumberPlotLabels() throws MacroExecutionException
    {
        CategoryPlot categoryPlot = mock(CategoryPlot.class);
        CategoryAxis categoryAxis = mock(CategoryAxis.class);
        NumberAxis rangeAxis = mock(NumberAxis.class);

        when(categoryPlot.getDomainAxis()).thenReturn(categoryAxis);
        when(categoryPlot.getRangeAxis()).thenReturn(rangeAxis);

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("categorylabelposition", "up45");
                    }
                },
                categoryPlot,
                null
        );
        verify(categoryAxis).setCategoryLabelPositions(CategoryLabelPositions.UP_45);

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("categorylabelposition", "up90");
                    }
                },
                categoryPlot,
                null
        );
        verify(categoryAxis).setCategoryLabelPositions(CategoryLabelPositions.UP_90);

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("categorylabelposition", "down45");
                    }
                },
                categoryPlot,
                null
        );
        verify(categoryAxis).setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("categorylabelposition", "down90");
                    }
                },
                categoryPlot,
                null
        );
        verify(categoryAxis).setCategoryLabelPositions(CategoryLabelPositions.DOWN_90);
    }

    @Test
    public void testHandleXyNumbersPlotCustomization() throws MacroExecutionException
    {
        XYPlot xyPlot = mock(XYPlot.class);
        NumberAxis domainAxis = mock(NumberAxis.class);
        NumberAxis rangeAxis = mock(NumberAxis.class);

        when(xyPlot.getDomainAxis()).thenReturn(domainAxis);
        when(xyPlot.getRangeAxis()).thenReturn(rangeAxis);

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("domainaxislabelangle", "45");
                        put("domainaxisrotateticklabel", "true");
                        put("domainaxislowerbound", String.valueOf(0));
                        put("domainaxisupperbound", String.valueOf(10));
                        put("domainaxistickunit", String.valueOf(1));
                        put("rangeaxislabelangle", "45");
                        put("rangeaxisrotateticklabel", "true");
                        put("rangeaxislowerbound", String.valueOf(0));
                        put("rangeaxisupperbound", String.valueOf(100));
                        put("rangeaxistickunit", String.valueOf(10));
                    }
                },
                xyPlot,
                null
        );

        verify(domainAxis).setLabelAngle(Math.toRadians(45));
        verify(domainAxis).setVerticalTickLabels(true);
        verify(domainAxis).setLowerBound(0);
        verify(domainAxis).setUpperBound(10);
        verify(domainAxis).setTickUnit(new NumberTickUnit(1));

        verify(rangeAxis).setLabelAngle(Math.toRadians(45));
        verify(rangeAxis).setVerticalTickLabels(true);
        verify(rangeAxis).setLowerBound(0);
        verify(rangeAxis).setUpperBound(100);
        verify(rangeAxis).setTickUnit(new NumberTickUnit(10));
    }

    @Test
    public void testHandleXyDatePlotCustomization() throws MacroExecutionException, ParseException
    {
        XYPlot xyPlot = mock(XYPlot.class);
        DateAxis domainAxis = mock(DateAxis.class);
        DateAxis rangeAxis = mock(DateAxis.class);
        ChartData chartData = mock(ChartData.class);

        when(xyPlot.getDomainAxis()).thenReturn(domainAxis);
        when(xyPlot.getRangeAxis()).thenReturn(rangeAxis);

        final DateFormat dateFormatOverride = new SimpleDateFormat("yyyy-MM-dd");
        when(chartData.getDateFormat(anyInt())).thenReturn(dateFormatOverride);

        when(chartData.toDate(anyString())).thenAnswer(
                (Answer<Date>) invocationOnMock -> dateFormatOverride.parse((String) invocationOnMock.getArguments()[0])
        );

        chartMacro.handleAxisCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("domainaxislabelangle", "90");
                        put("domainaxislowerbound", "1970-01-01");
                        put("domainaxisupperbound", "1980-01-01");
                        put("domainaxistickunit", "2");
                        put("datetickmarkposition", "start");

                        put("rangeaxislabelangle", "45");
                        put("rangeaxisrotateticklabel", "true");
                        put("rangeaxislowerbound", "1970-01-01");
                        put("rangeaxisupperbound", "1971-01-01");
                        put("rangeaxistickunit", "3");
                        put("timeperiod", "month");
                    }
                },
                xyPlot,
                chartData
        );

        verify(domainAxis).setDateFormatOverride(dateFormatOverride);
        verify(domainAxis).setLabelAngle(Math.toRadians(90));
        verify(domainAxis).setVerticalTickLabels(false);
        verify(domainAxis).setMinimumDate(new DateTime(1970, 1, 1, 0, 0, 0, 0).toDate());
        verify(domainAxis).setMaximumDate(new DateTime(1980, 1, 1, 0, 0, 0, 0).toDate());
        verify(domainAxis).setTickMarkPosition(DateTickMarkPosition.START);
        verify(domainAxis).setTickUnit(new DateTickUnit(DateTickUnit.MONTH, 2));

        verify(rangeAxis).setDateFormatOverride(dateFormatOverride);
        verify(rangeAxis).setLabelAngle(Math.toRadians(45));
        verify(rangeAxis).setVerticalTickLabels(true);
        verify(rangeAxis).setMinimumDate(new DateTime(1970, 1, 1, 0, 0, 0, 0).toDate());
        verify(rangeAxis).setMaximumDate(new DateTime(1971, 1, 1, 0, 0, 0, 0).toDate());
        verify(domainAxis).setTickMarkPosition(DateTickMarkPosition.START);
        verify(rangeAxis).setTickUnit(new DateTickUnit(DateTickUnit.MONTH, 3));
    }

    @Test
    public void testHandleOpacityCustomization() throws MacroExecutionException
    {
        Plot aPlot = mock(Plot.class);

        chartMacro.handleOpacityCustomization("50", aPlot);
        verify(aPlot).setForegroundAlpha(0.5f);
    }

    @Test
    public void testHandleCategoryPlotColorCustomization() throws MacroExecutionException
    {
        CategoryPlot aPlot = mock(CategoryPlot.class);
        CategoryItemRenderer categoryItemRenderer = mock(CategoryItemRenderer.class);

        when(aPlot.getRenderer()).thenReturn(categoryItemRenderer);

        chartMacro.handleColorCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("colors", "red, green, blue");
                    }
                },
                aPlot
        );

        verify(categoryItemRenderer).setSeriesPaint(0, new Color(0xff0000));
        verify(categoryItemRenderer).setSeriesPaint(1, new Color(0xff00));
        verify(categoryItemRenderer).setSeriesPaint(2, new Color(0xff));
    }

    @Test
    public void testHandleXyPlotPlotColorCustomization() throws MacroExecutionException
    {
        XYPlot aPlot = mock(XYPlot.class);
        XYItemRenderer xyItemRenderer = mock(XYItemRenderer.class);

        when(aPlot.getRenderer()).thenReturn(xyItemRenderer);

        chartMacro.handleColorCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("colors", "red, green, blue");
                    }
                },
                aPlot
        );

        verify(xyItemRenderer).setSeriesPaint(0, new Color(0xff0000));
        verify(xyItemRenderer).setSeriesPaint(1, new Color(0xff00));
        verify(xyItemRenderer).setSeriesPaint(2, new Color(0xff));
    }

    @Test
    public void testHandlePiePlotPlotColorCustomization() throws MacroExecutionException
    {
        PiePlot aPlot = mock(PiePlot.class);
        PieDataset dSet = mock(PieDataset.class);

        when(aPlot.getDataset()).thenReturn(dSet);
        when(dSet.getItemCount()).thenReturn(3);
        when(dSet.getKey(0)).thenReturn("first");
        when(dSet.getKey(1)).thenReturn("second");
        when(dSet.getKey(2)).thenReturn("third");

        chartMacro.handleColorCustomization(
                new HashMap<String, String>()
                {
                    {
                        put("colors", "red, green, blue");
                    }
                },
                aPlot
        );

        verify(aPlot, times(3)).getDataset();
        verify(aPlot).setSectionPaint("first", new Color(0xff0000));
        verify(aPlot).setSectionPaint("second", new Color(0xff00));
        verify(aPlot).setSectionPaint("third", new Color(0xff));
    }
}
