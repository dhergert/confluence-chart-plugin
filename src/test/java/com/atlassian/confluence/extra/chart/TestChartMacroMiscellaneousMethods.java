package com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.LanguageManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.links.LinkResolver;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.DateTickUnit;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.awt.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class TestChartMacroMiscellaneousMethods
{
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private SettingsManager settingsManager;
    @Mock
    private LanguageManager languageManager;
    @Mock
    private AttachmentManager attachmentManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ThumbnailManager thumbnailManager;
    @Mock
    private WritableDownloadResourceManager exportDownloadResourceManager;
    @Mock
    private XhtmlContent xhtmlContent;
    @Mock
    private LinkResolver linkResolver;
    @Mock
    private LocaleManager localeManager;
    @Mock
    private I18NBeanFactory i18nBeanFactory;
    @Mock
    private Attachment attachment;
    
    private ChartMacro chartMacro;

    private Map<String, Integer> colorMap = Collections.unmodifiableMap(new HashMap<String, Integer>()
    {
        {
            put("aqua", 0x00ffff);
            put("black", 0x000000);
            put("blue", 0x0000ff);
            put("cyan", 0x00ffff);
            put("fuchsia", 0xff00ff);
            put("gray", 0x808080);
            put("green", 0x00ff00);
            put("lime", 0x00ff00);
            put("maroon", 0x800000);
            put("navy", 0x000080);
            put("olive", 0x808000);
            put("purple", 0xffc0ff);
            put("red", 0xff0000);
            put("silver", 0xc0c0c0);
            put("teal", 0x808000);
            put("violet", 0xee82ee);
            put("white", 0xffffff);
            put("yellow", 0xffff00);
        }
    });

    private Map<String, Integer> dateTicks = Collections.unmodifiableMap(
            new HashMap<String, Integer>()
            {
                {
                    put("u", DateTickUnit.MILLISECOND);
                    put("s", DateTickUnit.SECOND);
                    put("m", DateTickUnit.MINUTE);
                    put("h", DateTickUnit.HOUR);
                    put("d", DateTickUnit.DAY);
                    put("M", DateTickUnit.MONTH);
                    put("y", DateTickUnit.YEAR);
                }
            }
    );

    @Before
    public void setUp() {
        chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager,
                thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory);
    }

    @Test
    public void testLowerCaseMacroParameters()
    {
        Map<String, String> macroParameters = new HashMap<>();
        Map<String, String> expectedTransformedMacroParameters = new HashMap<>();

        // Create random macro parameters
        for (int i = 0; i < 10; ++i)
        {
            String parameterName = StringUtils.upperCase(RandomStringUtils.randomAlphanumeric(8));

            macroParameters.put(parameterName, StringUtils.lowerCase(parameterName));
            expectedTransformedMacroParameters.put(StringUtils.lowerCase(parameterName), macroParameters.get(parameterName));
        }

        assertEquals(expectedTransformedMacroParameters, chartMacro.toLowerCase(macroParameters));
    }

    @Test
    public void testGetErrorPanelIncludesErrorMessage()
    {
        final String message = "Hello World";
        assertEquals(String.format("<div class=\"error\"><span class=\"error\">%s</span> </div>", message), chartMacro.getErrorPanel(message));
    }

    @Test
    public void testStringToColorWithLowerCaseNames() throws MacroExecutionException
    {
        for (String colorName : colorMap.keySet())
            assertEquals(
                    new Color(colorMap.get(colorName)),
                    chartMacro.stringToColor(StringUtils.lowerCase(colorName))
            );
    }

    @Test
    public void testStringToColorWithUpperCaseNames() throws MacroExecutionException
    {
        for (String colorName : colorMap.keySet())
            assertEquals(
                    new Color(colorMap.get(colorName)),
                    chartMacro.stringToColor(StringUtils.upperCase(colorName))
            );
    }

    @Test
    public void testStringToColorWithRbgHexColorValue() throws MacroExecutionException
    {
        assertEquals(
                new Color(0xffffff),
                chartMacro.stringToColor("#ffffff")
        );
    }

    @Test(expected = MacroExecutionException.class)
    public void testStringToColorWithInvalidColor() throws MacroExecutionException {
        chartMacro.stringToColor("invalid");
    }

    @Test
    public void testDateTickSpecification() throws MacroExecutionException
    {
        for (Map.Entry<String, Integer> dateTickPair : dateTicks.entrySet())
        {
            DateAxis dateAxis = new DateAxis();

            chartMacro.setDateTick(
                    Collections.emptyMap(),
                    dateAxis,
                    String.format("%d%s", 1, dateTickPair.getKey())
            );
            assertEquals(dateTickPair.getValue(), new Integer(dateAxis.getTickUnit().getUnit()));
        }
    }

    @Test(expected = MacroExecutionException.class)
    public void testInvalidDateTickSpecification() throws MacroExecutionException {
        DateAxis dateAxis = new DateAxis();
        chartMacro.setDateTick(
                Collections.emptyMap(),
                dateAxis,
                String.format("%dX", 1)
        );
    }

    @Test
    public void testDateTickSpecificationWithTimePeriodParameter() throws MacroExecutionException
    {
        Map<String, Map<Integer, Integer>> timeValueToCountMap = new HashMap<String, Map<Integer, Integer>>()
        {
            {
                put("year", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.YEAR, 1);
                    }
                });
                put("quarter", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.MONTH, 3);
                    }
                });
                put("month", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.MONTH, 1);
                    }
                });
                put("week", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.DAY, 7);
                    }
                });
                put("day", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.DAY, 1);
                    }
                });
                put("hour", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.HOUR, 1);
                    }
                });
                put("minute", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.MINUTE, 1);
                    }
                });
                put("second", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.SECOND, 1);
                    }
                });
                put("millisecond", new HashMap<Integer, Integer>()
                {
                    {
                        put(DateTickUnit.MILLISECOND, 1);
                    }
                });
            }
        };

        for (final Map.Entry<String, Map<Integer, Integer>> timeValueToCountPair : timeValueToCountMap.entrySet())
        {
            DateAxis dateAxis = new DateAxis();

            chartMacro.setDateTick(
                    new HashMap<String, String>()
                    {
                        {
                            put("timeperiod", timeValueToCountPair.getKey());
                        }
                    },
                    dateAxis,
                    String.valueOf(1));

            Map.Entry<Integer, Integer> tickToCountPair = timeValueToCountPair.getValue().entrySet().iterator().next();

            assertEquals(tickToCountPair.getKey(), new Integer(dateAxis.getTickUnit().getUnit()));
            assertEquals(tickToCountPair.getValue(), new Integer(dateAxis.getTickUnit().getCount()));
        }
    }

    @Test
    public void testParsingWikiMarkupInOldExecuteMethod() throws Exception
    {
    	Map<String, String> parameters = new HashMap<>();
    	Page testPage = new Page();
    	testPage.setSpace(new Space("TST"));
    	RenderContext renderContext = testPage.toPageContext();

    	String wikiMarkup = "{chart}\n" +
                "|| || Democrat || Republican || Independent ||\n" +
                "|| Mascots | 40 | 40 | 20 |\n" +
                "{chart}";

    	chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager, thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory)
    	{
    		Attachment getAttachment(Map<String, String> parameters, ConversionContext conversionContext, String chartDataHtml) {
    			return attachment;
    		}
    	};

    	when(settingsManager.getGlobalSettings()).thenReturn(new Settings());
    	when(attachment.getDownloadPath()).thenReturn(StringUtils.EMPTY);
    	
    	chartMacro.execute(parameters, wikiMarkup, renderContext);
    }

}
