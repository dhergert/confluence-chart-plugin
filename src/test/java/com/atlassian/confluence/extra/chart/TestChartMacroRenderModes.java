package com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;
import com.atlassian.confluence.languages.LanguageManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.TokenType;
import com.atlassian.renderer.links.LinkResolver;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class TestChartMacroRenderModes extends TestCase
{
    @Mock
    private SettingsManager settingsManager;

    @Mock
    private LanguageManager languageManager;

    @Mock
    private AttachmentManager attachmentManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private ThumbnailManager thumbnailManager;

    @Mock
    private WritableDownloadResourceManager exportDownloadResourceManager;

    @Mock
    private XhtmlContent xhtmlContent;

    @Mock
    private LinkResolver linkResolver;
    
    @Mock
    private LocaleManager localeManager;
    
    @Mock
    private I18NBeanFactory i18nBeanFactory;

    private ChartMacro chartMacro;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);
        chartMacro = new ChartMacro(settingsManager, languageManager, attachmentManager, permissionManager, thumbnailManager, exportDownloadResourceManager, xhtmlContent, linkResolver, localeManager, i18nBeanFactory);
    }

    @Override
    protected void tearDown() throws Exception
    {
        settingsManager = null;
        languageManager = null;
        attachmentManager = null;
        permissionManager = null;
        thumbnailManager = null;
        exportDownloadResourceManager = null;
        xhtmlContent = null;
        linkResolver = null;
        super.tearDown();
    }

    public void testChartMacroIsBlock()
    {
        assertEquals(TokenType.BLOCK, chartMacro.getTokenType(null, null, null));
        assertEquals(Macro.OutputType.BLOCK, chartMacro.getOutputType());
    }
}
