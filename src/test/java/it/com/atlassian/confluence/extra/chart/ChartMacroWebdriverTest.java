package it.com.atlassian.confluence.extra.chart;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.ContentType;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.service.content.AttachmentService;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.rpc.api.permissions.SpacePermission;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessTestRunner;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import com.atlassian.confluence.webdriver.pageobjects.ConfluenceTestedProduct;
import com.atlassian.confluence.webdriver.pageobjects.page.content.ViewPage;
import com.atlassian.fugue.Option;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.lift.Matchers;

import javax.inject.Inject;
import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

@RunWith(ConfluenceStatelessTestRunner.class)
public class ChartMacroWebdriverTest {
    @Inject
    private ConfluenceRestClient rest;

    @Inject
    private static ConfluenceTestedProduct product;

    @Inject
    private static PageElementFinder pageElementFinder;

    @Fixture
    private static UserFixture user = UserFixture.userFixture()
            .username("user")
            .build();

    @Fixture
    private static SpaceFixture space = SpaceFixture.spaceFixture()
            .namePrefix("spacey-space")
            .permission(user, SpacePermission.values())
            .build();

    @Fixture
    private static SpaceFixture otherSpace = SpaceFixture.spaceFixture()
            .namePrefix("other-space")
            .permission(user, SpacePermission.values())
            .build();

    @Fixture
    private static PageFixture pageWithChart = PageFixture.pageFixture()
            .title("Page with chart")
            .content("{chart:attachment=file.png}\n" +
                    "|| || Democrat || Republican || Independent ||\n" +
                    "|| Mascots | 40 | 40 | 20 |\n" +
                    "{chart}", ContentRepresentation.WIKI)
            .space(space)
            .author(user)
            .build();

    @Fixture
    private static PageFixture otherPage = PageFixture.pageFixture()
            .title("Another page with a chart")
            .content("{chart:attachment=Page with chart^anotherPage.png}\n" +
                    "|| || Democrat || Republican || Independent ||\n" +
                    "|| Mascots | 40 | 40 | 20 |\n" +
                    "{chart}", ContentRepresentation.WIKI)
            .space(space)
            .author(user)
            .build();

    @Fixture
    private static PageFixture pageWithChartWithOutDateRange = PageFixture.pageFixture()
            .title("Gant chart without date range")
            .content("{chart:type=gantt|orientation=vertical|3D=true|width=900|columns=2,,1,3,4}\n" +
                    "|| Person || Type || Start || End ||\n" +
                    "|| test | Stable | 29.12.2014 | 30.12.2014 |\n" +
                    "|| test1 | Stable | 25.07.2014 | 04.08.2014 |\n" +
                    "|| test2 | Crash | 22.12.201429.12.2014 | 23.12.201431.12.2014 |\n" +
                    "{chart}", ContentRepresentation.WIKI)
            .space(space)
            .author(user)
            .build();

    @Fixture
    private static PageFixture pageWithChartWithOutDateRange200Years = PageFixture.pageFixture()
            .title("Gant chart without date range 200 years")
            .content("{chart:attachment=file.png|type=gantt|orientation=vertical|3D=true|width=900|columns=2,,1,3,4}\n" +
                    "|| Person || Type || Start || End ||\n" +
                    "|| test | Stable | 29.12.2000 | 30.12.2200 |\n" +
                    "|| test1 | Stable | 25.07.2014 | 04.08.2014 |\n" +
                    "|| test2 | Crash | 22.12.2014 | 23.12.2014 |\n" +
                    "{chart}", ContentRepresentation.WIKI)
            .space(space)
            .author(user)
            .build();

    @Fixture
    private static PageFixture pageWithChartWithSpecialCharacter = PageFixture.pageFixture()
            .title("Chart with special character")
            .content("{chart:attachment=file.png|type=pie|dataOrientation=vertical}\n" +
                    "|| Name || Number ||\n" +
                    "| Ápples | 60 |\n" +
                    "| Oranges | 50 |\n" +
                    "{chart}", ContentRepresentation.WIKI)
            .space(space)
            .author(user)
            .build();

    @Fixture
    private static PageFixture pageWithChartWithHTMLEntities = PageFixture.pageFixture()
            .title("Chart with html entities")
            .content("<ac:structured-macro ac:name=\"chart\" ac:schema-version=\"1\" ac:macro-id=\"\">" +
                    "<ac:parameter ac:name=\"dataOrientation\">vertical</ac:parameter>" +
                    "<ac:parameter ac:name=\"attachment\">file.png</ac:parameter>" +
                    "<ac:rich-text-body>\n" +
                    "<table class=\"wrapped\">\n" +
                    "<tbody>\n" +
                    "<tr>\n" +
                    "<th>Name</th>\n" +
                    "<th>Number</th>" +
                    "</tr>\n" +
                    "<tr>\n" +
                    "<td><span>Ápples &Aacute;pples</span></td>\n" +
                    "<td>60</td>" +
                    "</tr>\n" +
                    "<tr>\n" +
                    "<td>O&nbsp;&reg;a&lt;n&gt;g&amp;e</td>\n" +
                    "<td>50</td>" +
                    "</tr>" +
                    "</tbody>" +
                    "</table>" +
                    "</ac:rich-text-body>" +
                    "</ac:structured-macro>", ContentRepresentation.STORAGE)
            .space(space)
            .author(user)
            .build();

    @Test
    public void simpleChart() {
        product.loginAndView(user.get(), pageWithChart.get());
        Poller.waitUntilTrue(pageElementFinder.find(By.cssSelector("div.wiki-content img")).timed().isPresent());
        final PageElement chart = pageElementFinder.find(By.cssSelector("div.wiki-content img"));
        assertTrue((chart.getAttribute("src").matches("^.*/download/attachments/.*/file.png.*")));
    }

    @Test
    public void simpleChartOutputsToAttachment() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        final ViewPage viewPage = product.loginAndView(user.get(), pageWithChart.get());

        /* Check if an attachment is created */
        final Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewPage.getPageId()))
                .withFilename("file.png")
                .fetchOne();

        assertTrue(attachment.isDefined());
        assertThat(Integer.parseInt(attachment.get().getExtension(AttachmentService.FILE_SIZE).toString()), Matchers.atLeast(1));
        assertThat(attachment.get().getExtension(AttachmentService.MEDIA_TYPE_METADATA_KEY), is("image/png"));
    }

    @Test
    public void createSimpleChartAndOutputToAttachmentOfAnotherPage() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        final ViewPage viewOtherPage = product.loginAndView(user.get(), otherPage.get());

        /* Check if an attachment is created */
        final Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(pageWithChart.get().getId())
                .withFilename("anotherPage.png")
                .fetchOne();

        assertTrue(attachment.isDefined());

        /* Check that the page which contains the macro does not have an attachment. */
        final Option<Content> ghostAttachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewOtherPage.getPageId()))
                .withFilename("anotherPage.png")
                .fetchOne();

        assertTrue(ghostAttachment.isEmpty());
    }

    @Test
    public void createSimpleChartAndOutputToAttachmentOfAnotherPageInAnotherSpace() {
        final Content otherPage = Content.builder()
                .space(otherSpace.get())
                .type(ContentType.PAGE)
                .title("Page with macro, but not attachment" + new Date())
                .body("{chart:attachment=" + space.get().getKey() + ":Page with chart^anotherPageAnotherSpace.png}\n" +
                        "|| || Democrat || Republican || Independent ||\n" +
                        "|| Mascots | 40 | 40 | 20 |\n" +
                        "{chart}", ContentRepresentation.WIKI)
                .build();
        rest.getAdminSession().contentService().create(otherPage);

        // Trigger the macro's execution so the chart gets saved to an attachment
        final ViewPage viewOtherPage = product.loginAndView(user.get(), otherPage);

        /* Check if an attachment is created */
        final Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(pageWithChart.get().getId())
                .withFilename("anotherPageAnotherSpace.png")
                .fetchOne();

        assertTrue(attachment.isDefined());

        /* Check that the page which contains the macro does not have an attachment. */
        final Option<Content> ghostAttachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewOtherPage.getPageId()))
                .withFilename("anotherPageAnotherSpace.png")
                .fetchOne();

        assertTrue(ghostAttachment.isEmpty());
    }

    @Test
    public void attachmentIsNotCreatedIfImageIsSame() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        ViewPage viewPage = product.loginAndView(user.get(), pageWithChart.get());

        /* Check that an attachment is created */
        Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewPage.getPageId()))
                .withFilename("file.png")
                .fetchOne();

        assertTrue(attachment.isDefined());
        assertThat(Integer.parseInt(attachment.get().getExtension(AttachmentService.FILE_SIZE).toString()), Matchers.atLeast(1));

        // reload the page and make sure no new version of attachment is created
        viewPage = product.viewPage(pageWithChart.get());

        attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewPage.getPageId()))
                .withFilename("file.png")
                .fetchOne();

        assertTrue(attachment.isDefined());
        assertThat(0, is(attachment.get().getAncestors().size()));
    }

    @Test
    public void ganttChartWithOutRangeDate() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        product.loginAndView(user.get(), pageWithChartWithOutDateRange.get());

        Poller.waitUntilTrue(pageElementFinder.find(By.cssSelector("span.error")).timed().isPresent());
        final PageElement error = pageElementFinder.find(By.cssSelector("span.error"));
        assertThat(error.getText(), is("Invalid input date at table row 'test2'"));
    }

    @Test
    public void ganttChartWithDateRange200Years() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        final ViewPage viewPage = product.loginAndView(user.get(), pageWithChartWithOutDateRange200Years.get());

        final Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewPage.getPageId()))
                .withFilename("file.png")
                .fetchOne();

        assertTrue(attachment.isDefined());
    }

    @Test
    public void chartWithSpecialCharacterShouldRender() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        final ViewPage viewPage = product.loginAndView(user.get(), pageWithChartWithSpecialCharacter.get());

        final Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewPage.getPageId()))
                .withFilename("file.png")
                .fetchOne();

        assertTrue(attachment.isDefined());
    }

    @Test
    public void chartWithHtmlEntitiesShouldRender() {
        // Trigger the macro's execution so the chart gets saved to an attachment
        final ViewPage viewPage = product.loginAndView(user.get(), pageWithChartWithHTMLEntities.get());

        final Option<Content> attachment = rest.getAdminSession().attachmentService().find()
                .withContainerId(ContentId.of(ContentType.PAGE, viewPage.getPageId()))
                .withFilename("file.png")
                .fetchOne();

        assertTrue(attachment.isDefined());
    }
}
