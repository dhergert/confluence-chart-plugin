package com.atlassian.confluence.extra.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieToolTipGenerator;
import org.jfree.chart.labels.StandardXYItemLabelGenerator;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.AbstractRenderer;
import org.jfree.chart.renderer.category.AreaRenderer;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.renderer.xy.XYStepAreaRenderer;
import org.jfree.chart.renderer.xy.XYStepRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.TableXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;
import org.jfree.util.Rotation;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Rectangle;
import java.text.NumberFormat;

/**
 * A collection of utility methods for creating some standard charts with JFreeChart.
 */
public abstract class ConfluenceChartFactory extends ChartFactory
{

    /*
     * @see {@link ChartFactory#createPieChart(String, PieDataset, boolean, boolean, boolean)}
     */
    public static JFreeChart createPieChart(String title, PieDataset dataset, boolean legend, boolean tooltips,
        boolean urls, boolean is3d)
    {
        if (is3d)
        {
            return createPieChart3D(title, dataset, legend, tooltips, urls);
        }
        else
        {
            return createPieChart(title, dataset, legend, tooltips, urls);
        }
    }

    /*
     * @see {@link ChartFactory#createPieChart(String, PieDataset, boolean, boolean, boolean)}
     */
    public static JFreeChart createPieChart(String title, PieDataset dataset, boolean legend, boolean tooltips,
        boolean urls)
    {
        JFreeChart chart = ChartFactory.createPieChart(title, dataset, legend, tooltips, urls);
        setPieChartDefaults(chart, dataset);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createPieChart3D(String, PieDataset, boolean, boolean, boolean)}
     */
    public static JFreeChart createPieChart3D(String title, PieDataset dataset, boolean legend, boolean tooltips,
        boolean urls)
    {
        JFreeChart chart = ChartFactory.createPieChart3D(title, dataset, legend, tooltips, urls);
        setPieChartDefaults(chart, dataset);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createBarChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createBarChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls,
        boolean is3d, boolean isStacked)
    {
        if (is3d && isStacked)
        {
            return createStackedBarChart3D(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls);
        }
        else if (isStacked)
        {
            return createStackedBarChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls);
        }
        else if (is3d)
        {
            return createBarChart3D(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls);
        }
        else
        {
            return createBarChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls);
        }
    }

    /*
     * @see {@link ChartFactory#createBarChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createBarChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createBarChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setBarChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createStackedBarChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createStackedBarChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createStackedBarChart(title, categoryAxisLabel, valueAxisLabel, dataset,
            orientation, legend, tooltips, urls);
        setBarChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createBarChart3D(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createBarChart3D(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createBarChart3D(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setBarChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createStackedBarChart3D(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createStackedBarChart3D(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createStackedBarChart3D(title, categoryAxisLabel, valueAxisLabel, dataset,
            orientation, legend, tooltips, urls);
        setBarChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createAreaChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createAreaChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls,
        boolean isStacked)
    {
        if (isStacked)
        {
            return createStackedAreaChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls);
        }
        else
        {
            return createAreaChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls);
        }
    }

    /*
     * @see {@link ChartFactory#createAreaChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createAreaChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createAreaChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setAreaChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createStackedAreaChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createStackedAreaChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createStackedAreaChart(title, categoryAxisLabel, valueAxisLabel, dataset,
            orientation, legend, tooltips, urls);
        setAreaChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createLineChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createLineChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls,
        boolean is3d, boolean showShapes)
    {
        if (is3d)
        {
            return createLineChart3D(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls, showShapes);
        }
        else
        {
            return createLineChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
                legend, tooltips, urls, showShapes);
        }
    }

    /*
     * @see {@link ChartFactory#createLineChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createLineChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        return createLineChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation, legend, tooltips, urls,
            true);
    }

    /*
     * @see {@link ChartFactory#createLineChart3D(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createLineChart3D(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        return createLineChart3D(title, categoryAxisLabel, valueAxisLabel, dataset, orientation, legend, tooltips, urls,
            true);
    }

    /*
     * @see {@link ChartFactory#createLineChart(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createLineChart(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls,
        boolean showShapes)
    {
        JFreeChart chart = ChartFactory.createLineChart(title, categoryAxisLabel, valueAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setLineChartDefaults(chart, showShapes);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createLineChart3D(String, String, String, CategoryDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createLineChart3D(String title, String categoryAxisLabel, String valueAxisLabel,
        CategoryDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls,
        boolean showShapes)
    {
        JFreeChart chart = ChartFactory.createLineChart3D(title, categoryAxisLabel, valueAxisLabel, dataset,
            orientation, legend, tooltips, urls);
        setLineChartDefaults(chart, showShapes);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createGanttChart(String, String, String, IntervalCategoryDataset, boolean, boolean, boolean)}
     */
    public static JFreeChart createGanttChart(String title, String categoryAxisLabel, String dateAxisLabel,
        IntervalCategoryDataset dataset, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createGanttChart(title, categoryAxisLabel, dateAxisLabel, dataset, legend,
            tooltips, urls);
        setGanttChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createScatterPlot(String, String, String, XYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createScatterPlot(String title, String xAxisLabel, String yAxisLabel, XYDataset dataset,
        PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createScatterPlot(title, xAxisLabel, yAxisLabel, dataset, orientation, legend,
            tooltips, urls);
        setScatterPlotDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createXYBarChart(String, String, boolean, String, IntervalXYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createXYBarChart(String title, String xAxisLabel, boolean dateAxis, String yAxisLabel,
        IntervalXYDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createXYBarChart(title, xAxisLabel, dateAxis, yAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setXYBarChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createXYAreaChart(String, String, String, XYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createXYAreaChart(String title, String xAxisLabel, String yAxisLabel, XYDataset dataset,
        PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createXYAreaChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend,
            tooltips, urls);
        setXYAreaChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createStackedXYAreaChart(String, String, String, TableXYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createStackedXYAreaChart(String title, String xAxisLabel, String yAxisLabel,
        TableXYDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createStackedXYAreaChart(title, xAxisLabel, yAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setXYAreaChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createXYLineChart(String, String, String, XYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createXYLineChart(String title, String xAxisLabel, String yAxisLabel, XYDataset dataset,
        PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend,
            tooltips, urls);
        setXYLineChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createXYStepChart(String, String, String, XYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createXYStepChart(String title, String xAxisLabel, String yAxisLabel, XYDataset dataset,
        PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createXYStepChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend,
            tooltips, urls);
        setXYStepChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createXYStepAreaChart(String, String, String, XYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createXYStepAreaChart(String title, String xAxisLabel, String yAxisLabel,
        XYDataset dataset, PlotOrientation orientation, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createXYStepAreaChart(title, xAxisLabel, yAxisLabel, dataset, orientation,
            legend, tooltips, urls);
        setXYStepAreaChartDefaults(chart);
        return chart;
    }

    /*
     * @see {@link ChartFactory#createTimeSeriesChart(String, String, String, XYDataset, PlotOrientation, boolean, boolean, boolean)}
     */
    public static JFreeChart createTimeSeriesChart(String title, String timeAxisLabel, String valueAxisLabel,
        XYDataset dataset, boolean legend, boolean tooltips, boolean urls)
    {
        JFreeChart chart = ChartFactory.createTimeSeriesChart(title, timeAxisLabel, valueAxisLabel, dataset, legend,
            tooltips, urls);
        setTimeSeriesChartDefaults(chart);
        return chart;
    }

    /**
     * Utility method to set the defaule style of the Pie Chart
     *
     * @param chart {@link JFreeChart} to style
     * @param dataset {@link PieDataset}
     */
    private static void setPieChartDefaults(JFreeChart chart, PieDataset dataset)
    {
        ChartUtil.setDefaults(chart);

        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setBackgroundPaint(ChartDefaults.transparent);
        plot.setOutlinePaint(ChartDefaults.transparent);
        plot.setCircular(true);
        plot.setDirection(Rotation.CLOCKWISE);
        plot.setIgnoreNullValues(true);
        plot.setIgnoreZeroValues(true);
        plot.setStartAngle(290);
        plot.setShadowXOffset(0.0);
        plot.setShadowYOffset(0.0);

        plot.setBaseSectionOutlinePaint(ChartDefaults.outlinePaintColor);
        plot.setBaseSectionOutlineStroke(new BasicStroke(2.0f));

        // tooltip generator
        plot.setToolTipGenerator(new StandardPieToolTipGenerator("{0} {1} ({2})"));

        // set the colors
        for (int j = 0; j < dataset.getItemCount(); j++)
        {
            if (j < ChartDefaults.darkColors.length && dataset.getValue(j).intValue() > 0)
            {
                if (ChartUtil.isVersion103Capable())
                {
                    plot.setSectionPaint(dataset.getKey(j), ChartDefaults.darkColors[j]);
                }
                else
                {
                    plot.setSectionPaint(j, ChartDefaults.darkColors[j]);
                }
            }
            else
            {
                break;
            }
        }

        // labels
        plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{0}"));
        plot.setLabelGap(0.04);
        plot.setLabelBackgroundPaint(ChartDefaults.transparent);
        plot.setLabelOutlinePaint(Color.gray.brighter());
        plot.setLabelShadowPaint(ChartDefaults.transparent);
        plot.setLabelFont(ChartDefaults.defaultFont);

        // legend
        plot.setLegendLabelGenerator(new StandardPieSectionLabelGenerator("{0} ({1} - {2})"));
        plot.setLegendItemShape(new Rectangle(0, 0, 10, 10));
    }

    /**
     * Utility method to set the default style of the Bar Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setBarChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();

        plot.setAxisOffset(new RectangleInsets(1.0, 1.0, 1.0, 1.0));

        // renderer
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelsVisible(false);

        renderer.setBasePositiveItemLabelPosition(
            new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BOTTOM_CENTER));
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);

        StandardCategoryToolTipGenerator generator =
            new StandardCategoryToolTipGenerator("{1}, {2}", NumberFormat.getInstance());
        renderer.setBaseToolTipGenerator(generator);
        renderer.setDrawBarOutline(false);
        renderer.setMaximumBarWidth(0.1);
        renderer.setItemMargin(0.02f);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the Area Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setAreaChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();

        // renderer
        AreaRenderer renderer = (AreaRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the Line Charts
     *
     * @param chart {@link JFreeChart} to style
     * @param showShapes indicate if shapes along the line will be shown
     */
    private static void setLineChartDefaults(JFreeChart chart, boolean showShapes)
    {
        ChartUtil.setDefaults(chart);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();

        // renderer
        LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
        renderer.setBaseShapesVisible(showShapes);
        renderer.setBaseStroke(ChartDefaults.defaultStroke);
        renderer.setStroke(ChartDefaults.defaultStroke);
    }

    /**
     * Utility method to set the default style of the Gantt Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setGanttChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        CategoryPlot plot = (CategoryPlot) chart.getPlot();
        plot.setRangeAxisLocation(AxisLocation.TOP_OR_LEFT);

        // renderer
        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the Scatter Plot
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setScatterPlotDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = (XYPlot) chart.getPlot();

        // renderer
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
            renderer.setSeriesStroke(j, ChartDefaults.defaultStroke);
        }
        renderer.setBaseStroke(ChartDefaults.defaultStroke);
    }

    /**
     * Utility method to set the default style of the XY Bar Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setXYBarChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = (XYPlot) chart.getPlot();

        plot.setAxisOffset(new RectangleInsets(1.0, 1.0, 1.0, 1.0));

        // renderer
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelsVisible(false);

        renderer.setBasePositiveItemLabelPosition(
            new ItemLabelPosition(ItemLabelAnchor.OUTSIDE12, TextAnchor.BOTTOM_CENTER));
        renderer.setBaseItemLabelGenerator(new StandardXYItemLabelGenerator());
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);

        StandardXYToolTipGenerator generator =
            new StandardXYToolTipGenerator("{1}, {2}", NumberFormat.getInstance(), NumberFormat.getInstance());
        renderer.setBaseToolTipGenerator(generator);
        renderer.setDrawBarOutline(false);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the XY Area Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setXYAreaChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = chart.getXYPlot();

        // renderer
        AbstractRenderer renderer = (AbstractRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the XY Line Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setXYLineChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = (XYPlot) chart.getPlot();

        // renderer
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        renderer.setBaseShapesVisible(false);
        renderer.setBaseStroke(ChartDefaults.defaultStroke);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesStroke(j, ChartDefaults.defaultStroke);
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the XY Step Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setXYStepChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setDomainGridlinesVisible(false);

        // renderer
        XYStepRenderer renderer = (XYStepRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseStroke(ChartDefaults.defaultStroke);
        renderer.setShapesVisible(false);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesStroke(j, ChartDefaults.defaultStroke);
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }
    }

    /**
     * Utility method to set the default style of the XY Step Area Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setXYStepAreaChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setDomainGridlinesVisible(false);

        // renderer
        XYStepAreaRenderer renderer = (XYStepAreaRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        renderer.setShapesVisible(false);
        renderer.setBaseStroke(ChartDefaults.defaultStroke);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesStroke(j, ChartDefaults.defaultStroke);
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
        }

        StandardXYToolTipGenerator generator =
            new StandardXYToolTipGenerator("{1}, {2}", NumberFormat.getInstance(), NumberFormat.getInstance());
        renderer.setBaseToolTipGenerator(generator);
    }

    /**
     * Utility method to set the default style of the Time Series Charts
     *
     * @param chart {@link JFreeChart} to style
     */
    private static void setTimeSeriesChartDefaults(JFreeChart chart)
    {
        ChartUtil.setDefaults(chart);

        XYPlot plot = (XYPlot) chart.getPlot();

        // renderer
        XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) plot.getRenderer();
        renderer.setBaseItemLabelFont(ChartDefaults.defaultFont);
        renderer.setBaseItemLabelPaint(ChartDefaults.axisLabelColor);
        for (int j = 0; j < ChartDefaults.darkColors.length; j++)
        {
            renderer.setSeriesPaint(j, ChartDefaults.darkColors[j]);
            renderer.setSeriesStroke(j, ChartDefaults.defaultStroke);
        }
        renderer.setBaseShapesVisible(false);
        renderer.setBaseStroke(ChartDefaults.defaultStroke);
    }
}
